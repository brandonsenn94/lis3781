select 'drop, create, use database, create tables, display data:' as '';
do sleep(5);

DROP SCHEMA IF EXISTS bgs19c;
CREATE SCHEMA IF NOT EXISTS bgs19c;
SHOW WARNINGS;
USE bgs19c;

-- person table

DROP TABLE IF EXISTS person;
CREATE TABLE IF NOT EXISTS person
(
    per_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_ssn BINARY(64) NULL,
    per_salt BINARY(64) NULL,
    per_fname VARCHAR(15) NOT NULL,
    per_lname VARCHAR(30) NOT NULL,
    per_city VARCHAR(30) NOT NULL,
    per_state CHAR(2) NOT NULL,
    per_zip INT(9) UNSIGNED ZEROFILL NOT NULL,
    per_email VARCHAR(100) NOT NULL,
    per_dob DATE NOT NULL,
    per_type ENUM('a', 'c', 'j') NOT NULL,
    per_notes VARCHAR(255) NULL,
    PRIMARY KEY (per_id)
    UNIQUE INDEX ux_per_ssn (per_ssn ASC)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- attorney table

DROP TABLE IF EXISTS attorney;
CREATE TABLE IF NOT EXISTS attorney
(
    per_id SMALLINT UNSIGNED NOT NULL,
    aty_start_date DATE NOT NULL,
    aty_end_date DATE NULL DEFAULT NULL,
    aty_hourly_rate DECIMAL(5,2) UNSIGNED NOT NULL,
    aty_years_in_practice TINYINT NOT NULL,
    aty_notes VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY(per_id),

    INDEX idx_per_id(per_id asc),

    CONSTRAINT fk_attorney_person
    FOREIGN KEY (per_id)
    REFERENCES person (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- client table

DROP TABLE IF EXISTS client;
CREATE TABLE IF NOT EXISTS client
(
    per_id SMALLINT UNSIGNED NOT NULL,
    cli_notes VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (per_id),

    INDEX idx_per_id(per_id asc),

    CONSTRAINT fk_client_person
    FOREIGN KEY (per_id)
    REFERENCES person (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- table court

DROP TABLE IF EXISTS court;
CREATE TABLE IF NOT EXISTS court
(
    crt_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    crt_name VARCHAR(45) NOT NULL,
    crt_street VARCHAR(30) NOT NULL,
    crt_city VARCHAR(30) NOT NULL,
    crt_state CHAR(2) NOT NULL,
    crt_zip INT(9) UNSIGNED ZEROFILL NOT NULL,
    crt_phone BIGINT NOT NULL,
    crt_email VARCHAR(100) NOT NULL,
    crt_url VARCHAR(100) NOT NULL,
    crt_notes VARCHAR(255) NULL,
    PRIMARY KEY (crt_id)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE utf8mb4_0900_ai_ci;

SHOW WARNINGS;


-- table judge

DROP TABLE IF EXISTS judge;
CREATE TABLE IF NOT EXISTS judge
(
    per_id SMALLINT UNSIGNED NOT NULL,
    crt_id TINYINT UNSIGNED NULL DEFAULT NULL,
    jud_salary DECIMAL(8,2) NOT NULL,
    jud_years_in_practice TINYINT UNSIGNED NOT NULL,
    jud_notes VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (per_id),

    INDEX idx_per_id (per_id ASC),
    INDEX idx_crt_id (crt_id ASC),

    CONSTRAINT fk_judge_person
    FOREIGN KEY (per_id)
    REFERENCES person (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,

    CONSTRAINT fk_judge_court
    FOREIGN KEY (crt_id)
    REFERENCES court(crt_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- judge_hist table

DROP TABLE IF EXISTS judge_hist;
CREATE TABLE IF NOT EXISTS judge_hist
(
    jhs_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED,
    jhs_crt_id TINYINT NULL,
    jhs_date timestamp NOT NULL default current_timestamp(),
    jhs_type enum('i', 'u', 'd') NOT NULL default 'i',
    jhs_salary DECIMAL(8,2) NOT NULL,
    jhs_notes VARCHAR(255) NULL,
    PRIMARY KEY (jhs_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_judge_hist_judge
    FOREIGN KEY (per_id)
    REFERENCES judge (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- case table

DROP TABLE IF EXISTS `case`;
CREATE TABLE IF NOT EXISTS `case`
(
    cse_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    cse_type VARCHAR(45) NOT NULL,
    cse_description TEXT NOT NULL,
    cse_start_date DATE NOT NULL,
    cse_end_date DATE NULL,
    cse_notes VARCHAR(255) NULL,
    PRIMARY KEY (cse_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_court_case_judge
    FOREIGN KEY (per_id)
    REFERENCES judge(per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- bar table

DROP TABLE IF EXISTS bar;
CREATE TABLE IF NOT EXISTS bar
(
    bar_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    bar_name VARCHAR(45) NOT NULL,
    bar_notes VARCHAR(255) NULL,
    PRIMARY KEY (bar_id),

    INDEX idx_per_id (per_id ASC),
    
    CONSTRAINT fk_bar_attorney
    FOREIGN KEY (per_id)
    REFERENCES attorney(per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- specialty table

DROP TABLE IF EXISTS specialty;
CREATE TABLE IF NOT EXISTS specialty
(
    spc_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    spc_type VARCHAR(45) NOT NULL,
    spc_notes VARCHAR(255) NULL,
    PRIMARY KEY (spc_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_specialty_attorney
    FOREIGN KEY (per_id)
    REFERENCES attorney(per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- assignment table

DROP TABLE IF EXISTS assignment;
CREATE TABLE IF NOT EXISTS assignment
(
    asn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_cid SMALLINT UNSIGNED NOT NULL,
    per_aid SMALLINT UNSIGNED NOT NULL,
    cse_id SMALLINT UNSIGNED NOT NULL,
    asn_notes VARCHAR(255) NULL,
    PRIMARY KEY (asn_id),

    INDEX idx_per_cid (per_cid ASC),
    INDEX idx_per_aid (per_aid ASC),
    INDEX idx_cse_id (cse_id ASC),

    UNIQUE INDEX ux_per_cid_per_aid_cse_id (per_cid ASC, per_aid ASC, cse_id ASC),

    CONSTRAINT fk_assign_case
    FOREIGN KEY (cse_id)
    REFERENCES `case`(cse_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,

    CONSTRAINT fk_assignment_client
    FOREIGN KEY (per_cid)
    REFERENCES client (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,

    CONSTRAINT fk_assignment_attorney
    FOREIGN KEY (per_aid)
    REFERENCES attorney (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- PHONE TABLE

DROP TABLE IF EXISTS phone;
CREATE TABLE IF NOT EXISTS phone
(
    phn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    phn_num BIGINT UNSIGNED NOT NULL,
    phn_type ENUM('h', 'c', 'w', 'f') NOT NULL COMMENT 'home cell work fax',
    phn_notes VARCHAR(255) NULL,
    PRIMARY KEY (phn_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_phone_person
    FOREIGN KEY (per_id)
    REFERENCES person (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE utf8mb4_0900_ai_ci;

SHOW WARNINGS;

SHOW TABLES;


-- INSERT DATA

START TRANSACTION;

INSERT INTO person
(per_id, per_ssn, per_salt, per_fname, per_lname, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(NULL, NULL, NULL, 'Steve', 'Rogers', 'Halibut', 'RI', 1000432432, 'srogers@channel10news.com', '1923-10-03', 'c', NULL),
(NULL, NULL, NULL, 'Bruce', 'Wayne', 'New Haven', 'MA', 200322322, 'test@gmail.com', '1943-10-03', 'c', NULL),
(NULL, NULL, NULL, 'Peter', 'Parker', 'Stockton', 'IL', 610855234, 'pparker@marvel.com', '1973-10-03', 'c', NULL),
(NULL, NULL, NULL, 'Jane', 'Thompson', 'Tallahassee', 'FL', 323013433, 'jthompson@gmail.com', '1993-10-03', 'c', NULL),
(NULL, NULL, NULL, 'Debra', 'Steele', 'New York', 'NY', 100015433, 'debsteele@nypd.com', '1996-03-31', 'c', NULL),
(NULL, NULL, NULL, 'Tony', 'Hawk', 'Clarksville', 'TN', 432301334, 'thawk@birdhouse.com', '1943-03-31', 'a', NULL),
(NULL, NULL, NULL, 'Hank', 'Pymi', 'Minneapolis', 'MN', 543531232, 'hank@pymi.com', '1986-01-23', 'a', NULL),
(NULL, NULL, NULL, 'Bob', 'Best', 'Santa Ana', 'CA', 921023343, 'bob@bobdole.com', '1989-10-03', 'a', NULL),
(NULL, NULL, NULL, 'Sandra', 'Dole', 'San Diego', 'CA', 921020030, 'sandra@bobdole.com', '1999-10-03', 'a', NULL),
(NULL, NULL, NULL, 'Ben', 'Avery', 'Cabo', 'CA', 932020202, 'ben@caborealestate.com', '1967-10-03', 'a', NULL),
(NULL, NULL, NULL, 'Arthur', 'Curry', 'Edmonton', 'CA', 921023233, 'ac123@saccounty.gov', '1976-10-03', 'j', NULL),
(NULL, NULL, NULL, 'Diana', 'Price', 'Seattle', 'WA', 999123222, 'diana.price@gmail.com', '1987-10-03', 'j', NULL),
(NULL, NULL, NULL, 'Adam', 'Jurris', 'Portland', 'OR', 983239334, 'jurrisadam@portland.gov', '1976-10-03', 'j', NULL),
(NULL, NULL, NULL, 'Judy', 'Sleen', 'Chicago', 'IL', 609033434, 'jsleen@cookcounty.gov', '1956-10-03', 'j', NULL),
(NULL, NULL, NULL, 'Bill', 'Neiderheim', 'Toledo', 'OH', 523431234, 'bill@elitefts.com', '1966-10-03', 'j', NULL);

COMMIT;

-- create SSN function

DROP PROCEDURE IF EXISTS CreatePersonSSN;
DELIMITER $$
CREATE PROCEDURE CreatePersonSSN()
BEGIN 
    DECLARE x, y INT;
    SET x = 1;

    SELECT count(*) into y from person;

    WHILE x <= y DO
        SET @salt = RANDOM_BYTES(64);
        SET @ran_num = FLOOR(RAND()*(999999999-111111111+1))+111111111;
        SET @ssn=unhex(sha2(concat(@salt, @ran_num), 512));

        UPDATE person
        SET per_ssn=@ssn, per_salt=@salt
        where per_id = x;
    
        SET x = x+1;
        END WHILE;

END $$
DELIMITER ;
CALL CreatePersonSSN();

START TRANSACTION;

INSERT INTO phone
(phn_id, per_id, phn_num, phn_type, phn_notes)
VALUES
(NULL, 1, 8032288827, 'c', NULL),
(NULL, 2, 8159080334, 'h', NULL),
(NULL, 3, 1235435343, 'w', '2 numbers'),
(NULL, 4, 6543654365, 'w', NULL),
(NULL, 5, 8948329488, 'f', 'fax'),
(NULL, 6, 8534985938, 'c', 'home phone preferred'),
(NULL, 7, 8995783493, 'h', 'call during lunch'),
(NULL, 8, 6546894869, 'w', 'secretary'),
(NULL, 9, 6543654365, 'f', 'fax'),
(NULL, 10, 4765437547, 'h', 'cell phone preferred'),
(NULL, 11, 6543654365, 'w', NULL),
(NULL, 12, 5643246364, 'w', 'secretary'),
(NULL, 13, 5643665464, 'c', NULL),
(NULL, 14, 5632343212, 'f', NULL),
(NULL, 15, 7583475878, 'h', 'afternoons');

COMMIT;

START TRANSACTION;

-- delete 3rd record from phone table due to video.

START TRANSACTION;
DELETE FROM phone
WHERE phn_id = 3;
COMMIT;

select * from phone;

-- client data

START TRANSACTION;

INSERT INTO client
(per_id, cli_notes)
VALUES
(1, NULL),
(2, NULL),
(3, NULL),
(4, NULL),
(5, NULL);

COMMIT;

-- atty data

START TRANSACTION;

INSERT INTO attorney
(per_id, aty_start_date, aty_end_date, aty_hourly_rate, aty_years_in_practice, aty_notes)
VALUES
(6, '2006-06-12', NULL, 85, 5, NULL),
(7, '2003-08-20', NULL, 130, 28, NULL),
(8, '2009-12-12', NULL, 70, 17, NULL),
(9, '2008-06-08', NULL, 78, 13, NULL),
(10, '2011-09-12', NULL, 60, 24, NULL);

COMMIT;

-- bar data

START TRANSACTION;

INSERT INTO bar
(bar_id, per_id, bar_name, bar_notes)
VALUES
(NULL, 6, 'Florida bar', NULL),
(NULL, 7, 'Alabama bar', NULL),
(NULL, 8, 'Georgia bar', NULL),
(NULL, 9, 'Michigan bar', NULL),
(NULL, 10, 'South Carolina bar', NULL),
(NULL, 6, 'Montana bar', NULL),
(NULL, 7, 'Arizona bar', NULL),
(NULL, 8, 'Nevada bar', NULL),
(NULL, 9, 'New York bar', NULL),
(NULL, 10, 'New York bar', NULL),
(NULL, 6, 'Mississippi bar', NULL),
(NULL, 7, 'California bar', NULL),
(NULL, 8, 'Illinois bar', NULL),
(NULL, 9, 'Wisconsin bar', NULL),
(NULL, 10, 'Indiana bar', NULL),
(NULL, 6, 'Illinois bar', NULL),
(NULL, 7, 'Ocala bar', NULL),
(NULL, 8, 'Chicago bar', NULL),
(NULL, 9, 'Texas bar', NULL);

COMMIT;

-- specialty data

START TRANSACTION;

INSERT INTO specialty
(spc_id, per_id, spc_type, spc_notes)
VALUES
(NULL, 6, 'business', NULL),
(NULL, 7, 'traffic', NULL),
(NULL, 8, 'bankruptcy', NULL),
(NULL, 9, 'insurance', NULL),
(NULL, 10, 'judicial', NULL),
(NULL, 6, 'environmental', NULL),
(NULL, 7, 'criminal', NULL),
(NULL, 8, 'real estate', NULL),
(NULL, 9, 'malpractice', NULL);

COMMIT;




-- court data
START TRANSACTION;

INSERT INTO court
(crt_id, crt_name, crt_street, crt_city, crt_state, crt_zip, crt_phone, crt_email, crt_url, crt_notes)
VALUES
(NULL, 'Leon County Circuit Court', '301 S Monroe St', 'Tallahassee', 'FL', 323015455, 8504399223, 'lccc@us.fl.gov', 'www.leoncountycourt.gov', NULL),
(NULL, 'Leon County Traffic Court', '1921 Thomasville Rd', 'Tallahassee', 'FL', 323015463, 8501232532, 'lctrafficcourt@us.fl.gov', 'www.leoncountytrafficcourt.gov', NULL),
(NULL, 'Florida Supreme Court', '500 S Duval St', 'Tallahassee', 'FL', 323015435, 8504432412, 'supremecourt@us.fl.gov', 'www.floridasupremecourt.gov', NULL),
(NULL, 'Orange County Courthouse', '424 N Orange Ave', 'Orlando', 'FL', 328015455, 4074329223, 'orangecountycourt@us.fl.gov', 'www.orangecountycourt.gov', NULL),
(NULL, 'Fifth District Court of Appeal', '300 South Beach', 'Daytona Beach', 'FL', 321158382, 4071238979, '5dca@us.fl.gov', 'www.fifthcircuitcourt.gov', NULL);

COMMIT;

-- judge data

START TRANSACTION;

INSERT INTO judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
VALUES
(11, 5, 150000, 10, NULL),
(12, 4, 185000, 3, NULL),
(13, 4, 135000, 2, NULL),
(14, 3, 170000, 6, NULL),
(15, 1, 120000, 1, NULL);

COMMIT;

-- judge history data

START TRANSACTION;

INSERT INTO judge_hist
(jhs_id, per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
VALUES
(NULL, 11, 3, '2009-01-16', 'i', 130000, NULL),
(NULL, 12, 2, '2010-05-27', 'i', 140000, NULL),
(NULL, 13, 5, '2000-01-02', 'i', 115000, NULL),
(NULL, 13, 4, '2005-07-05', 'i', 135000, NULL),
(NULL, 14, 4, '2008-12-09', 'i', 155000, NULL),
(NULL, 15, 1, '2011-03-17', 'i', 120000, NULL),
(NULL, 11, 5, '2010-07-05', 'i', 150000, NULL),
(NULL, 12, 4, '2012-10-08', 'i', 165000, NULL),
(NULL, 14, 3, '2009-04-19', 'i', 170000, NULL);

COMMIT;

-- case data

START TRANSACTIOn;

INSERT INTO `case`
(cse_id, per_id, cse_type, cse_description, cse_start_date, cse_end_date, cse_notes)
VALUES
(NULL, 13, 'civil', 'client says his logo is being used without consent.', '2010-10-09', NULL, 'copyright infringement'),
(NULL, 12, 'criminal', 'client charged with assault.', '2009-11-18', '2010-12-23', 'assault'),
(NULL, 14, 'civil', 'client broke ankle at grocery store.', '2008-05-06', '2008-07-23', 'slip and fall'),
(NULL, 11, 'criminal', 'client charged with grand theft.', '2011-05-20', NULL, 'grand theft'),
(NULL, 13, 'criminal', 'client charged with drug posession', '2011-06-05', NULL, 'drug posession'),
(NULL, 14, 'civil', 'client says newspaper printed false information.', '2009-01-19', '2009-05-20', 'defamation'),
(NULL, 12, 'criminal', 'client charged with 1st degree murder.', '2011-03-20', '2013-05-23', 'homicide'),
(NULL, 15, 'civil', 'client is declaring bankruptcy.', '2010-01-26', '2011-02-28', 'bankruptcy');

COMMIT;

START TRANSACTION;

INSERT INTO assignment
(asn_id, per_cid, per_aid, cse_id, asn_notes)
VALUES
(NULL, 1, 6, 7, NULL),
(NULL, 2, 6, 6, NULL),
(NULL, 3, 7, 2, NULL),
(NULL, 4, 8, 2, NULL),
(NULL, 5, 9, 5, NULL),
(NULL, 1, 10, 1, NULL),
(NULL, 2, 6, 3, NULL),
(NULL, 3, 7, 8, NULL),
(NULL, 4, 8, 8, NULL),
(NULL, 5, 9, 8, NULL),
(NULL, 4, 10, 4, NULL);

COMMIT;

/* Finally done inserting data. Took hours. W -_- W*/
-- question 1.

DROP VIEW if exists v_attorney_info;
CREATE VIEW v_attorney_info AS 
    SELECT 
    CONCAT(per_lname, ", ", per_fname) as name,
    CONCAT(per_state, ", ", per_state, " ", per_zip) as address,
    TIMESTAMPDIFF(year, per_dob, now()) as age,
    CONCAT('$', FORMAT(aty_hourly_rate, 2)) as hourly_rate,
    bar_name, spc_type
    from person
        natural join attorney
        natural join bar
        natural join specialty
    order by per_lname;

-- 2.

DROP PROCEDURE IF EXISTS sp_num_judges_born_by_month
DELIMITER //
CREATE PROCEDURE sp_num_judges_born_by_month()
BEGIN
    select month(per_dob) as month, monthname(per_dob) as month_name, count(*) as count
    from person
        natural join judge
    group by month_name, month --getting an error in 
    order by month;
END //
DELIMITER ;

CALL sp_num_judges_born_by_month();


--3. 

DROP PROCEDURE IF EXISTS sp_cases_and_judges;
DELIMITER //
CREATE PROCEDURE sp_cases_and_judges()
BEGIN
  select per_id, cse_id, cse_type, cse_description,
    concat(per_fname, " ", per_lname) as name,
    concat("(", substring(phn_num, 1, 3), ")", " ", substring(phn_num, 4, 3), "-", substring(phn_num, 7, 4)) as judge_office_number,
    phn_type,
    jud_years_in_practice, 
    cse_start_date,
    cse_end_date
  from person
    natural join judge
    natural join `case`
    natural join phone
  where per_type = 'j'
  order by per_lname;

END //
DELIMITER ;


--4. 

DROP TRIGGER IF EXISTS trg_judge_history_after_insert;
DELIMITER //
CREATE TRIGGER trg_judge_history_after_insert;
AFTER INSERT on judge
FOR EACH ROW
begin
    INSERT INTO judge_hist
    (per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
    VALUES
    (new.per_id, NEW.crt_id, current_timestamp(), 'i', NEW.jud_salary,
    concat("modifying user: ", user(), " Notes: ", NEW.jud_notes)
    );
end //
DELIMITER ;

--5. 

DROP TRIGGER IF EXISTS trg_judge_history_after_update;
DELIMITER //
CREATE TRIGGER trg_judge_history_after_update
AFTER UPDATE ON judge
FOR EACH ROW
begin
    INSERT INTO judge_hist
    (per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
    VALUES
    (new.per_id, new.crt_id, current_timestamp(), 'u', NEW.jud_salary,
    concat("modifying user: ", user(), " Notes: ", NEW.jud_notes)
    );
end //
DELIMITER ;

--6. 

DROP procedure if exists sp_add_judge_record;
DELIMITER //

CREATE PROCEDURE sp_add_judge_record()
begin
    INSERT INTO judge    
    (per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
    VALUES
    (6, 1, 110000, 0, concat("New judge was former attorney. ", "Modifying event creator: " , current_user())
    );
end //

DELIMITER ;


--ec

DROP EVENT IF EXISTS remove_judge_history;

DELIMITER //

CREATE EVENT IF NOT EXISTS remove_judge_history
ON SCHEDULE
    EVERY 2 MONTH
STARTS NOW() + INTERVAL 3 WEEK
ENDS NOW () + INTERVAL 4 year
COMMENT 'keeps only first 100 records.'
do
begin
  DELETE FROM JUDGE_HIST WHERE JHS_ID > 100;
end //

DELIMITER ;

