# LIS4381 - Advanced Database Management    

## Brandon Senn 

### Project 1 Requirements:
- ERD Screenshots
- [SQL Code Link(click me)](sql/lis3781_p1_solutions.sql)

--- 

|  ERD Screenshot 1  |  ERD Screenshot 2  |
|  ---  |  ---  |
| ![ERD Part 1](img/ERD_1.png) | ![ERD Part 2](img/ERD_2.png) |



