# LIS4381 - Advanced Database Management

## Brandon Senn 

### Assignment 5 Requirements:
#### Deliverables:
- ERD Diagram
- [Link to SQL Code](https://bitbucket.org/brandonsenn94/lis3781/src/master/a5/sql/a5_solution.sql)

| ERD (Part 1) | ERD (Part 2) | ERD (Part 3) | 
| --- | --- | --- |
| ![ERD Part 1](img/sql/ERD1.png) | ![ERD Part 2](img/sql/ERD2.png) | ![ERD Part 3](img/sql/ERD1.png) | 

