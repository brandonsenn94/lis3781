# Advanced Database Management

## Brandon Senn 

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/brandonsenn94/lis3781/src/master/a1/README.md)

    * Install Ampps w/ screenshot
    * Git commands w/ short descriptions
    * A1 ERD.
    * Bitbucket Repo links
        * This assignment 
        * Completed tutorial repo [BitbucketStationLocations](https://bitbucket.org/brandonsenn94/bitbucketstationlocations/src)    

2. [A2 README.md](https://bitbucket.org/brandonsenn94/lis3781/src/master/a2/README.md)
    * Tables Created and Populated (MySQL)
    * SQL Code Screenshots

3. [A3 README.md](https://bitbucket.org/brandonsenn94/lis3781/src/master/a3/README.md)
    * Tables Created and Populated (Oracle)
    * SQL Code Screenshots

4. [A4 README.md](https://bitbucket.org/brandonsenn94/lis3781/src/master/a4/README.md)
    * ERD
    * SQL Code Link

5. [A5 README.md](https://bitbucket.org/brandonsenn94/lis3781/src/master/a5/README.md)
    * ERD 
    * SQL Code Link

6. [P1 README.md](https://bitbucket.org/brandonsenn94/lis3781/src/master/p1/README.md)
    * ERD Diagram Screenshots

7. [P2 README.md](https://bitbucket.org/brandonsenn94/lis3781/src/master/p2/README.md)
    * MongoDB command
    * JSON for required reports