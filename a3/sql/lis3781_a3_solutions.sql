DROP SEQUENCE seq_cus_id;

CREATE SEQUENCE seq_cus_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

drop table customer CASCADE CONSTRAINTS PURGE;

CREATE TABLE customer
(
    cus_id number not null,
    cus_fname varchar2(15) not null,
    cus_lname varchar2(30) not null,
    cus_street varchar2(30) not null,
    cus_city varchar2(30) not null,
    cus_state char(2) not null,
    cus_zip number(9) not null,
    cus_phone number(10) not null,
    cus_email varchar2(100),
    cus_balance number(7,2),
    cus_notes varchar2(255),
    CONSTRAINT pk_customer PRIMARY KEY(cus_id)
);

DROP SEQUENCE seq_com_id;

CREATE SEQUENCE seq_com_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

drop table commodity CASCADE CONSTRAINTS PURGE;

CREATE TABLE commodity
(
    com_id number not null,
    com_name varchar2(20),
    com_price NUMBER(8,2) NOT NULL,
    cus_notes varchar2(255),
    CONSTRAINT pk_commodity PRIMARY KEY(com_id),
    CONSTRAINT uq_com_name UNIQUE(com_name)
);

DROP SEQUENCE seq_order_id;
CREATE SEQUENCE seq_order_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

DROP TABLE "order" CASCADE CONSTRAINTS PURGE;

CREATE TABLE "order"
(
    ord_id number(4,0) not null,
    cus_id number,
    com_id number,
    ord_num_units number(5,0) not null,
    ord_total_cost number(8,2) not null,
    ord_notes varchar2(255),
    CONSTRAINT pk_order PRIMARY KEY (ord_id),
    CONSTRAINT fk_order_customer
    FOREIGN KEY (cus_id)
    REFERENCES customer(cus_id),
    CONSTRAINT fk_order_commodity
    FOREIGN KEY (com_id)
    REFERENCES commodity(com_id),
    CONSTRAINT check_unit CHECK(ord_num_units > 0),
    CONSTRAINT check_total CHECK(ord_total_cost > 0)
);

-- populate customer table
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Tom', 'Scott', '543 Main Ave', 'Toledo', 'OH', 23433, 3132123797, 'tscott321@gmail.com', 1232.10, 'leaving');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Arnold', 'Palmer', '432 Alligator Ave', 'Orlando', 'FL', 32301, 8159431923, 'palmer@gmail.com', 23000.42, 'golf legend');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Brandon', 'Senn', '434 Apalachee Pkwy', 'Tallahassee', 'FL', 32301, 7813238698, 'bsenn@fsu.edu', 23.23, 'student');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Mikie', 'Davis', '123 1st St', 'New York', 'NY', 01123, 8152344848, 'mdavis92@gmail.com', 125.56, 'stellar customer');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Mark', 'Jowett', '636 Willow Way', 'Panama City', 'FL', 32205, 8501239599, 'mjowett@fsu.edu', 232.23, 'All caring instructor');
commit;

INSERT INTO commodity VALUES (seq_com_id.nextval, 'Cigarettes', 9.99, 'Virginia Reds');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Video Games', 89.99, 'Forza');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Oatmeal', 9.99, 'Triple milled steel cut oats');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Milk', 3.99, 'Great Value');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Transfer Case', 1099.99, '2005 Ford F-150');
commit;

INSERT INTO "orders" VALUES (seq_order_id.nextval, 1, 2, 50, 200, NULL);
INSERT INTO "orders" VALUES (seq_order_id.nextval, 2, 3, 30, 100, NULL);
INSERT INTO "orders" VALUES (seq_order_id.nextval, 3, 1, 6, 654, NULL);
INSERT INTO "orders" VALUES (seq_order_id.nextval, 5, 4, 24, 972, NULL);
INSERT INTO "orders" VALUES (seq_order_id.nextval, 3, 5, 7, 72, NULL);
INSERT INTO "orders" VALUES (seq_order_id.nextval, 1, 2, 5, 15, NULL);
INSERT INTO "orders" VALUES (seq_order_id.nextval, 2, 3, 75, 1200, NULL);
INSERT INTO "orders" VALUES (seq_order_id.nextval, 3, 1, 4, 300, NULL);
INSERT INTO "orders" VALUES (seq_order_id.nextval, 5, 4, 20, 20000.62, NULL);
INSERT INTO "orders" VALUES (seq_order_id.nextval, 1, 3, 785, 125000, NULL);
commit;

-- solutions for a3.
--1. display oracle version
select * from PRODUCT_COMPONENT_VERSION;

--2. alternative metod of Oracle version
select * from V$VERSION;
-- 3rd way:
select * FROM v$version where banner LIKE 'Oracle%';

--3. display current user

select user from dual;

--4. display date/time

SELECT TO_CHAR(SYSDATE, 'MM-DD-YYYY HH24:MI:SS') "NOW"
FROM DUAL;

SELECT TO_CHAR(SYSDATE, 'MM-DD-YYYY HH12:MI:SS AM') "NOW"
FROM DUAL;

--5. display user privileges
SELECT * FROM USER_SYS_PRIVS;

--6. display all user tables
SELECT OBJECT_NAME
FROM USER_OBJECTS
WHERE OBJECT_TYPE = 'TABLE';

SELECT TABLE_NAME
FROM USER_TABLES;

SELECT owner, TABLE_NAME
FROM all_tables
where owner='bgs19c';

--7. display table structures
describe customer;
describe commodity;
describe "order";

select * from customer;
select * from commodity;
select * from "order";

--8. list cust number, lastname, firstname, email from customer table;

select cus_id, cus_lname, cus_fname, cus_email
from customer;

-- 9.Same query as above, include street, city, state, and sortby state in descending order, and last name in ascending order.
select cus_id, cus_lname, cus_fname, cus_email, cus_street, cus_city, cus_state
from customer
sort by cus_state desc, cus_lname asc;

-- 10.What is the full name of customer number 3? Display last name first.
select cus_lname, cus_fname
from customer
where cus_id = 3;

-- 11.Find the customer number, last name, first name, and current balance for every customer whose balance exceeds $1,000, sorted by largest to smallest balances.
SELECT cus_id, cus_lname, cus_fname, cus_balance
from customer
where cus_balance > 1000
order by cus_balance desc;

-- 12.List the name of every commodity, and its price (formatted to two decimal places, displaying $ sign), sorted by smallest to largest price.
select com_name, to_char(com_price, 'L99,999.99') as price_formatted
from commodity
order by com_price;

--13. 
SELECT (cus_lname || ', ' || cus_fname) as name,
(cus_street || ', ' || cus_city || ', ' || cus_state || '' || cus_zip) as address
from customer
order by cus_zip desc;

--14. 
select * from "order"
where com_id not in (select com_id from commodity where lower(com_name)='cereal');

select * from "order"
where com_id  != (select com_id from commodity where lower(com_name)='cereal');

select * from "order"
where com_id <> (select com_id from commodity where lower(com_name)='cereal');

--15. 
select cus_id, cus_lname, cus_fname, to_char(cus_balance, $99,999.99) as balance_formatted
from customer
where cus_balance >= 500 and cus_balance <=1000;

select cus_id, cus_lname, cus_fname, to_char(cus_balance, L99,999.99) as balance_formatted
from customer
where cus_balance >= 500 and cus_balance <=1000;

select cus_id, cus_lname, cus_fname, to_char(cus_balance, L99,999.99) as balance_formatted
from customer
where cus_balance BETWEEN 500 and <=1000;

--16.
select cuS_id, cus_lname, to_char(cus_balance, L99,999.99) as balance_formatted
from customer
where cus_balance >
(select avg(cus_balance) from customer);


--17.
select cus_id, cus_lname, cus_fname, to_char(sum(ord_total_cost), 'L99,999.99') as 'total orders'
from customer
natural join "order"
group by cus_id, cus_lname, cus_fname
order by sum(ord_total_cost) desc;

--18. 

--19. 

select cus_id, cus_lname, cus_fname, to_char(sum(ord_total_cost), 'L99,999.99') as 'total orders'
from customer
natural join "order"
group by cus_id, cus_lname, cus_fname
having sum(ord_total_cost) > 1500
order by sum(ord_total_cost) desc;

--20. 

select cus_id, cus_lname, cus_fname, ord_num_units
from customer
natural join "order"
where ord_num_units IN (30, 40, 50);

--21.

select cus_id, cus_lname, cus_fname,
count(*) as 'number of orders'
to_char(min(ord_total_cost), 'L99,999.99') as 'minimum order cost'
to_char(max(ord_total_cost), 'L99,999.99') as 'maximum order cost'
to_char(sum(ord_total_cost), 'L99,999.99') as 'total orders'
from customer
natural join "order"
where exists(select * from customer having count(*) >= 5)
group by cus_id, cus_lname, cus_fname;

--22.
select count(*),
count(cus_balance),
sum(cus_balance),
avg(cus_balance),
max(cus_balance),
min(cus_balance)
from customer;

--23.
select distinct count(distinct cus_id) from "order";

select distinct cus_id from "order";

--24.

select cu.cus_id, cus_lname, cus_fname, com_name, ord_id, to_char(ord_total_cost, 'L99,999.99') as 'order amount'
from customer cu
join "order" o on o.cus_id = cu.cus_id
join commodity co on co.com_id = o.com_id
order by ord_total_cost desc;

--25.
SET DEFINE OFF

UPDATE commodity
SET com_price = 99
WHERE com_name = 'DVD & Player';










