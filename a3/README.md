# LIS2781 - Advanced Database Management

## Brandon Senn 

### Assignment 3 Requirements:
- Tables Created and Populated in Oracle Remote Server.
- SQL Code

| Customer Table | Commodity Table | Order Table |
| --- | --- | --- |
| ![Customer Table](img/a3_customerTable.png) | ![Customer Table](img/a3_commodityTable.png) | ![Customer Table](img/a3_orderTable.png) |

| SQL Code (1) | SQL Code (2) | SQL Code (3) |
| --- | --- | --- |
| ![SQL Code 1](img/a3_sqlCode_1.png) | ![SQL Code 2](img/a3_sqlCode_2.png) | ![SQL Code ](img/a3_sqlCode_3.png) |