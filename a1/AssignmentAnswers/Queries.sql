-- 1.
select emp_id, emp_fname, emp_lname,
CONCAT(emp_street, ", ", emp_city, ", ", emp_state, " ", substring(emp_zip, 1,5), '-', substring(emp_zip, 6,4)) as address,
CONCAT('(', substring(emp_phone, 1,3), ')', substring(emp_phone, 4, 3),'-', substring(emp_phone, 7, 4)) as phone_num,
CONCAT(substring(emp_ssn, 1, 3), '-', substring(emp_ssn, 4, 2), '-', substring(emp_ssn, 6,4)) as emp_ssn, job_title
from job as j, employee as e
where j.job_id = e.job_id
order by emp_lname desc;

-- 2. 
select e.emp_id, emp_fname, emp_lname, eht_date, eht_job_id, job_title, eht_emp_salary, eht_notes
from employee e, emp_hist h, job j
where e.emp_id = h.emp_id
and h.eht_job_id = j.job_id
order by emp_id, eht_date;

select e.emp_id, emp_fname, emp_lname, eht_date, eht_job_id, job_title, eht_emp_salary, eht_notes
from employee e, emp_hist h, job j
where e.emp_id = h.emp_id
and h.eht_job_id = j.job_id
order by emp_id, eht_date;

describe emp_hist;

-- 3. 

SELECT 
    emp_fname,
    emp_lname,
    emp_dob,
    TIMESTAMPDIFF(YEAR, emp_dob, CURDATE()) AS emp_age,
    dep_fname,
    dep_lname,
    dep_relation,
    dep_dob,
    TIMESTAMPDIFF(YEAR, dep_dob, CURDATE()) AS dep_age
FROM
    employee
        NATURAL JOIN
    dependent
ORDER BY emp_lname;

-- 4.

START TRANSACTION;
	SELECT * FROM job;
    
    UPDATE JOB
    SET JOB_title = 'owner'
    WHERE job_id = 1;
    
    SELECT * from job;
COMMIT;

-- 5.

DROP PROCEDURE IF EXISTS insert_benefit;
DELIMITER //
CREATE PROCEDURE insert_benefits()

BEGIN 
	SELECT * from benefit;
    
    INSERT INTO benefit
    (ben_name, ben_notes)
    VALUES 
    ('new benefit', 'testing');
    
    SELECT * from benefit;
END //

DELIMITER ;

CALL INSERT_benefits();
DROP PROCEDURE IF EXISTS insert_benefits;

-- 6. 

SELECT emp_id,
		concat(emp_lname, ", ", emp_fname) as employee,
        concat(substring(emp_ssn, 1,3), '-', substring(emp_ssn, 4, 2), '-', substring(emp_ssn, 6, 4)) as emp_ssn,
        emp_email as email,
		concat(dep_lname, ", ", dep_fname) as dependent,
        concat(substring(dep_ssn, 1,3), '-', substring(dep_ssn, 4, 2), '-', substring(dep_ssn, 6, 4)) as dep_ssn,
        concat(dep_street, ", ", emp_city, ", ", dep_state, " ", substring(dep_zip, 1, 5), '-', substring(dep_zip, 6, 4)) as address,
        concat('(', substring(dep_phone, 1, 3), ')', substring(dep_phone, 4, 3), '-', substring(dep_phone, 7, 4)) as phone_num
FROM employee
	NATURAL LEFT OUTER JOIN dependent
ORDER BY emp_lname;

-- xtra

SELECT emp_fname, emp_lname, emp_dob,
DATE_FORMAT(NOW(), '%Y') - 
DATE_FORMAT(emp_dob, '%Y') -
(DATE_FORMAT(NOW(), '00-%m-%d') <
DATE_FORMAT(emp_dob, '00-%m-%d')) AS emp_age
FROM employee;

-- 7.
DROP TRIGGER IF EXISTS trg_employee_after_insert;

delimiter //

create trigger trg_employee_after_insert;
	AFTER INSERT on employee
    FOR EACH ROW
    BEGIN
    INSERT INTO emp_hist
			(emp_id, eht_date, eht_type, eht_job_id, eht_emp_salary, eht_usr_changed, eht_reason, eht_notes)
	VALUES
    (NEW.emp_id, now(), 'i', NEW.job_id, NEW.emp_salary, user(), "new employee", NEW.emp_notes);
    END //
DELIMITER ;
