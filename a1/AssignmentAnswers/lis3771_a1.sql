-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema bgs19c
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `bgs19c` ;

-- -----------------------------------------------------
-- Schema bgs19c
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bgs19c` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `bgs19c` ;

-- -----------------------------------------------------
-- Table `bgs19c`.`job`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bgs19c`.`job` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bgs19c`.`job` (
  `job_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_title` VARCHAR(45) NOT NULL,
  `job_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`job_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `bgs19c`.`employee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bgs19c`.`employee` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bgs19c`.`employee` (
  `emp_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_id` TINYINT UNSIGNED NOT NULL,
  `emp_ssn` INT(9) UNSIGNED NOT NULL,
  `emp_fname` VARCHAR(15) NOT NULL,
  `emp_lname` VARCHAR(30) NOT NULL,
  `emp_dob` DATE NOT NULL,
  `emp_start_date` DATE NOT NULL,
  `emp_end_date` DATE NULL,
  `emp_salary` DECIMAL(8,2) NOT NULL,
  `emp_street` VARCHAR(30) NOT NULL,
  `emp_city` VARCHAR(20) NOT NULL,
  `emp_state` CHAR(2) NOT NULL,
  `emp_zip` INT(9) UNSIGNED NOT NULL,
  `emp_phone` BIGINT UNSIGNED NOT NULL,
  `emp_email` VARCHAR(100) NOT NULL,
  `emp_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`emp_id`),
  INDEX `fk_employee_job_idx` (`job_id` ASC),
  CONSTRAINT `fk_employee_job`
    FOREIGN KEY (`job_id`)
    REFERENCES `bgs19c`.`job` (`job_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `bgs19c`.`benefit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bgs19c`.`benefit` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bgs19c`.`benefit` (
  `ben_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ben_name` VARCHAR(45) NOT NULL,
  `ben_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`ben_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `bgs19c`.`plan`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bgs19c`.`plan` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bgs19c`.`plan` (
  `pln_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NOT NULL,
  `ben_id` TINYINT UNSIGNED NOT NULL,
  `pln_type` ENUM('single', 'spouse', 'family') NOT NULL,
  `pln_cost` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pln_election_date` DATE NOT NULL,
  `pln_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pln_id`),
  INDEX `fk_plan_employee1_idx` (`emp_id` ASC),
  INDEX `fk_plan_benefit1_idx` (`ben_id` ASC),
  CONSTRAINT `fk_plan_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `bgs19c`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_plan_benefit1`
    FOREIGN KEY (`ben_id`)
    REFERENCES `bgs19c`.`benefit` (`ben_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `bgs19c`.`emp_hist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bgs19c`.`emp_hist` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bgs19c`.`emp_hist` (
  `eht_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NULL,
  `eht_date` DATETIME NOT NULL,
  `eht_type` ENUM('i', 'u', 'd') NOT NULL,
  `eht_job_id` TINYINT UNSIGNED NOT NULL,
  `eht_emp_salary` DECIMAL(8,2) UNSIGNED NOT NULL,
  `eht_usr_change` VARCHAR(30) NOT NULL,
  `eht_reason` VARCHAR(45) NOT NULL,
  `eht_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`eht_id`),
  INDEX `fk_emp_hist_employee1_idx` (`emp_id` ASC),
  CONSTRAINT `fk_emp_hist_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `bgs19c`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `bgs19c`.`dependent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bgs19c`.`dependent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bgs19c`.`dependent` (
  `dep_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NOT NULL,
  `dep_added` DATE NOT NULL,
  `dep_ssn` INT(9) UNSIGNED NOT NULL,
  `dep_fname` VARCHAR(15) NOT NULL,
  `dep_lname` VARCHAR(30) NOT NULL,
  `dep_dob` DATE NOT NULL,
  `dep_relation` VARCHAR(20) NOT NULL,
  `dep_street` VARCHAR(30) NOT NULL,
  `dep_city` VARCHAR(20) NOT NULL,
  `dep_state` CHAR(2) NOT NULL,
  `dep_zip` INT(9) UNSIGNED NOT NULL,
  `dep_phone` BIGINT NOT NULL,
  `dep_email` VARCHAR(100) NOT NULL,
  `dep_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`dep_id`),
  INDEX `fk_dependent_employee1_idx` (`emp_id` ASC),
  CONSTRAINT `fk_dependent_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `bgs19c`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `bgs19c`.`job`
-- -----------------------------------------------------
START TRANSACTION;
USE `bgs19c`;
INSERT INTO `bgs19c`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (1, 'Vice President', NULL);
INSERT INTO `bgs19c`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (2, 'Janitor', 'test');
INSERT INTO `bgs19c`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (3, 'Software Developer', NULL);
INSERT INTO `bgs19c`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (4, 'Professor', 'test');
INSERT INTO `bgs19c`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (5, 'CEO', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bgs19c`.`employee`
-- -----------------------------------------------------
START TRANSACTION;
USE `bgs19c`;
INSERT INTO `bgs19c`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (1, 1, 432156543, 'Tony', 'Hernandez', '2002-12-03', '2018-05-05', NULL, 55000.23, '1232 w. main', 'New York', 'NY', 00001, 5432542343, 'donotreply@brandtinfo.com', 'test');
INSERT INTO `bgs19c`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (2, 2, 846904567, 'Drake', 'Draken', '1994-09-26', '2001-09-11', '2020-01-10', 75123.43, '5435 west st', 'Tallahassee', 'FL', 32301, 5432543543, 'donotreply@google.com', 'est');
INSERT INTO `bgs19c`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (3, 3, 123546987, 'Martha', 'Stewart', '1994-09-27', '2010-10-15', NULL, 105003.53, '1232 w 1st ave', 'Denver', 'CO', 82001, 7543754375, 'donotreply@microsoft.com', 't');
INSERT INTO `bgs19c`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (4, 4, 747892392, 'Snoop', 'Dogg', '2000-01-05', '2015-09-10', '2015-12-31', 25000.00, '543 hillside ln', 'Los Angeles', 'CA', 90210, 0876087608, 'donotreply@saxonglobal.com', NULL);
INSERT INTO `bgs19c`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (5, 5, 743742342, 'Charles', 'Manson', '2001-03-04', '2013-05-05', NULL, 10523.23, '123 main st', 'Seattle', 'WA', 78439, 0876087605, 'donotreply@snp.com', 'est');
INSERT INTO `bgs19c`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (6, 2, 125430293, 'Rob', 'Gronkowski', '1990-03-15', '2013-05-15', NULL, 1000.23, '643 main st', 'Portland', 'OR', 43278, 8126544354, 'donotreply@brandtinfo.com', NULL);
INSERT INTO `bgs19c`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (7, 1, 543234643, 'Brandon', 'senn', '1994-09-26', '2005-10-13', '2007-12-01', 54353.23, '5325 main st', 'San Diego', 'CA', 54325, 7543776554, 'donotreply@brandtinfo.com', NULL);
INSERT INTO `bgs19c`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (8, 3, 654374374, 'Mark', 'jowett', '1967-07-13', '1990-01-13', NULL, 12343.43, '5435 w main st', 'Juneau', 'AK', 16433, 1234657645, 'donotreply@brandtinfo.com', 'est');
INSERT INTO `bgs19c`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (9, 4, 543252533, 'Cathy', 'Green', '1974-09-26', '2000-09-26', '2001-10-31', 500000.62, '5354 south ave', 'Portland', 'ME', 54356, 2543276346, 'donotreply@brandtinfo.com', NULL);
INSERT INTO `bgs19c`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (10, 5, 784688696, 'Karen', 'testing', '1963-03-31', '1995-10-15', NULL, 543254.62, '5435 capital cir', 'Anchorage', 'AK', 76546, 6734765486, 'donotreply@brandtinfo.com', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bgs19c`.`benefit`
-- -----------------------------------------------------
START TRANSACTION;
USE `bgs19c`;
INSERT INTO `bgs19c`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (1, 'single', NULL);
INSERT INTO `bgs19c`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (2, 'family', NULL);
INSERT INTO `bgs19c`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (3, 'spouse', NULL);
INSERT INTO `bgs19c`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (4, 'family', NULL);
INSERT INTO `bgs19c`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (5, 'single', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bgs19c`.`plan`
-- -----------------------------------------------------
START TRANSACTION;
USE `bgs19c`;
INSERT INTO `bgs19c`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (1, 1, 1, 'single', 100.00, '2020-01-01', '');
INSERT INTO `bgs19c`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (2, 2, 2, 'family', 400.00, '2020-01-01', NULL);
INSERT INTO `bgs19c`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (3, 3, 3, 'single', 100.00, '2020-01-01', NULL);
INSERT INTO `bgs19c`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (4, 4, 4, 'spouse', 200.00, '2020-01-01', NULL);
INSERT INTO `bgs19c`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (5, 5, 5, 'single', 100.00, '2020-01-01', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bgs19c`.`emp_hist`
-- -----------------------------------------------------
START TRANSACTION;
USE `bgs19c`;
INSERT INTO `bgs19c`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_change`, `eht_reason`, `eht_notes`) VALUES (1, 1, '2020-01-01', 'i', 1, 54352.12, 'Janitor', 'Fired', NULL);
INSERT INTO `bgs19c`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_change`, `eht_reason`, `eht_notes`) VALUES (2, 2, '2020-01-10', 'i', 2, 51534.33, 'Data Entry', 'Promoted', NULL);
INSERT INTO `bgs19c`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_change`, `eht_reason`, `eht_notes`) VALUES (3, 3, '1994-01-01', 'u', 3, 73454.34, 'Journalist', 'Fired', NULL);
INSERT INTO `bgs19c`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_change`, `eht_reason`, `eht_notes`) VALUES (4, 4, '2021-01-01', 'u', 4, 97659.62, 'HR', 'Promoted', NULL);
INSERT INTO `bgs19c`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_change`, `eht_reason`, `eht_notes`) VALUES (5, 5, '2020-12-15', 'd', 5, 763443.23, 'Editor', 'Transfered', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bgs19c`.`dependent`
-- -----------------------------------------------------
START TRANSACTION;
USE `bgs19c`;
INSERT INTO `bgs19c`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (1, 1, '2020-08-19', 118590734, 'Oswell', 'Bouzan', '1998-12-19', 'Isuzu', '2 Clove Terrace', 'Seattle', 'WA', 11604, 2065982198, 'obouzan0@gnu.org', '');
INSERT INTO `bgs19c`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (2, 2, '2020-01-10', 439169517, 'Brandais', 'Latore', '1991-02-13', 'Audi', '6 Riverside Court', 'Oklahoma City', 'OK', 32077, 4053856589, 'blatore1@blogtalkradio.com', '');
INSERT INTO `bgs19c`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (3, 3, '2020-05-20', 410939157, 'Raynor', 'Glander', '1996-09-01', 'Volkswagen', '5181 Meadow Valley Court', 'Palm Bay', 'FL', 30232, 5617969328, 'rglander2@cafepress.com', '');
INSERT INTO `bgs19c`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (4, 4, '2020-05-29', 427739661, 'Gerhard', 'O\' Loughran', '1983-11-05', 'Hyundai', '4 Lyons Street', 'Fort Worth', 'TX', 88409, 6824835508, 'goloughran3@intel.com', '');
INSERT INTO `bgs19c`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (5, 5, '2020-11-20', 537522990, 'Ricky', 'Abrahami', '1990-02-01', 'GMC', '83 Morningstar Plaza', 'Peoria', 'IL', 53836, 3091051155, 'rabrahami4@facebook.com', '');

COMMIT;

