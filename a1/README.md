# LIS3781 - Advanced Database Management

## Brandon Senn 

### Assignment 1 Requirements:

*Course Work Links:*

### README.md file should include the following items:
* Screenshot of AMPPS Installation
* git commands w/ short descriptions
* ERD Screenshot
* Bitbucket repo links; a) This assignment b) BitbucketStationLocations

### Git Commands with Short Descriptions
1. git init - creates a new Git repo
2. git status - displays the state of the working directory and staging area
3. git add - adds files to the staging area for files
4. git commit - used to create a snapshot of the staged changes along a timeline of a Git project's history
5. git push - Used to upload local repository content to a remote repository
6. git pull - Used to download files from a remote repository
7. git rm - used to remove files from a git repository
    
### Assignment Screenshots

1. AMPPS install/PHP Information

![PHP Information](https://bitbucket.org/brandonsenn94/lis4381/raw/44cbd503bb4dcf68d0999529e97c40a7deaec005/A1/res/PHP%20info.png)



### Tutorial Links: 
1. BitbucketTutorial - Station Locations [A1 BitbucketStationLocations](https://bitbucket.org/brandonsenn94/bitbucketstationlocations)

