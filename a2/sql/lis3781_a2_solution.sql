drop database if exists bgs19c;
create database if not exists bgs19c;
use bgs19c;

-- company table
DROP TABLE if exists company;
CREATE TABLE if  NOT exists company
(
    cmp_id INT unsigned NOT NULL AUTO_INCREMENT,
    cmp_type enum('C-Corp', 'S-Corp', 'Non-Profit-Corp', 'LLC', 'Partnership'),
    cmp_street VARCHAR(30) NOT NULL,
    cmp_city VARCHAR(30) NOT NULL,
    cmp_state CHAR(2) NOT NULL,
    cmp_zip int(9) unsigned ZEROFILL NOT NULL comment 'no dashes',
    cmp_phone bigint unsigned NOT NULL,
    cmp_ytd_sales DECIMAL(10, 2) unsigned NOT NULL,
    cmp_email VARCHAR(100) NULL,
    cmp_url VARCHAR(100) NULL,
    cmp_notes VARCHAR(255) NULL,
    PRIMARY KEY(cmp_id)
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

INSERT INTO company
VALUES
(null, 'C-Corp', '507 - 20 Ave. E. Apt. 2A', 'Seattle', 'WA','081226749', '2065559857', '12345678.00', NULL, 'https://website.com', null),
(null, 'S-Corp', '9078 Capital Way', 'Tacoma', 'WA','053542343', '4345598357', '15435.12', NULL, 'https://google.com', null),
(null, 'Non-Profit-Corp', '232 w. main st', 'Stockton', 'IL','610852322', '8159080448', '1433.23', NULL, 'https://amazon.com', null),
(null, 'LLC', '1600 Pennsylvania Ave', 'Washington', 'DC','543521232', '5345325123', '12378.12', NULL, 'https://chewy.com', 'initial push'),
(null, 'Partnership', '5354 Apalachee Pkwy', 'Tallahassee', 'FL','323013232', '7347542345', '155678.10', NULL, 'https://yreee.com', null);

SHOW WARNINGS;

-- cust table

DROP TABLE if exists customer;
CREATE TABLE if NOT EXISTS customer
(
    cus_id INT unsigned NOT NULL AUTO_INCREMENT,
    cmp_id INT unsigned NOT NULL,
    cus_ssn binary(64) not null,
    cus_hash binary(64) not null,
    cus_type enum('Loyal', 'Discount', 'Impulse', 'Need-Based', 'Wandering'),
    cus_first varchar(15) NOT NULL,
    cus_last varchar(30) NOT NULL,
    cus_street varchar(30) NULL,
    cus_city varchar(30) NULL,
    cus_state CHAR(2) NULL,
    cus_zip int(9) unsigned ZEROFILL NULL,
    cus_phone bigint unsigned NOT NULL,
    cus_email VARCHAR(100) NULL,
    cus_balance DECIMAL(7,2) unsigned NULL,
    cus_tot_sales DECIMAL(7,2) unsigned NULL,
    cus_notes varchar(255) NULL,
    PRIMARY KEY (cus_id),

    UNIQUE INDEX ux_cus_ssn(cus_ssn asc),
    INDEX idx_cmp_id (cmp_id asc),

    CONSTRAINT fk_customer_company
    FOREIGN KEY(cmp_id)
    REFERENCES company(cmp_id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

set @salt=RANDOM_BYTES(64);

INSERT INTO customer
VALUES
(null, 1, unhex(SHA2(CONCAT(@salt, '000456789'), 512)), @salt, 'Discount', 'Wilbur', 'Denaway', '23 Billings Gate', 'El Paso', 'TX', '074378394', '2145431234', 'test@testing.com', '9381.43', '54353.12', 'cust notes'),
(null, 2, unhex(SHA2(CONCAT(@salt, '342123532'), 512)), @salt, 'Loyal', 'Tannis', 'Jasder', '12 Main St', 'Warren', 'IL', '610871232', '6108712322', 'test@google.com', '12.43', '54332.12', NULL),
(null, 3, unhex(SHA2(CONCAT(@salt, '535435434'), 512)), @salt, 'Impulse', 'Greg', 'Doucette', '434 Hillside Ln', 'Stockton', 'IL', '610851232', '8159080448', 'test@apple.com', '5431.23', '54325.12', NULL),
(null, 4, unhex(SHA2(CONCAT(@salt, '754231233'), 512)), @salt, 'Need-Based', 'Arnold', 'Scharz', '2855 Apalachee Pkwy', 'Tallahassee', 'FL', '323015432', '8501232532', 'test@amazon.com', '5432.13', '554.22', 'cust notes'),
(null, 5, unhex(SHA2(CONCAT(@salt, '654634543'), 512)), @salt, 'Wandering', 'Tom', 'Platz', '543 Tennessee St', 'Tallahassee', 'FL', '323015323', '8501232222', 'test@amazon.com', '12003.53', '92.33', NULL);

SHOW WARNINGS;

/* Creating User 1 w/ permissions*/
CREATE USER 'user1'@'localhost' IDENTIFIED BY 'password';

GRANT SELECT, UPDATE, DELETE
ON bgs19c.*
TO 'user1'@'localhost';

/*User 2 creation/granting permissions*/
CREATE USER 'user2'@'localhost' IDENTIFIED BY 'Passw0rd1';

GRANT SELECT, INSERT
ON bgs19c.customer
TO 'user2'@'localhost';

